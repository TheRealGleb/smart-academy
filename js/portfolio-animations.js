

jQuery(document).ready(function() {
	$(".sub-menu li").hover(function() {
		$("img", this).replaceWith('<img src="img/portfolio/slash.png" style="height: 20px; width:15px; margin-left:15px;">');
	}, function() {
		$(this).removeClass('hover-social-media');
		$("img", this).replaceWith('<img src="img/portfolio/list-style-img.png">');
	});

	var elements = [];

	$(".project-picture").each( function(index, element) {
		elements.push($(element).attr("id"));
	});

	$(".project-picture").hover(function() {
		for (var i = 1; i <= elements.length; i++) {
			if ($(this).attr("id") == "index-"+i) {
				var hover = "#hover-"+i;
				$(hover).addClass('opacity-1');
			} else continue
		}
	}, function() {
		$(".hover-view").removeClass('opacity-1');
	});
});