

jQuery(document).ready(function() {
	$(".search").hover(function() {
		$("img", this).replaceWith('<img src="img/mainPage/search-icon-hover.png">');
	}, function() {
		$("img", this).replaceWith('<img src="img/mainPage/search-icon.png">');
	});

	$(".search").hover(function() {
		$(".searching").addClass('visible');
	}, function() {
		$(".searching").removeClass('visible');
	});

	$(".change-lang").click(function() {
		$(".lang-changer").addClass('visible');
	});

	$(".lang-changer > div").click(function() {
		$(".lang-changer").removeClass('visible');
	});


	$(".social-icon-linkedIn").hover(function() {
		$(this).addClass('hover-social-media');
		$("img", this).replaceWith('<img src="img/mainPage/linkedIn-icon-hover.png">');
	}, function() {
		$(this).removeClass('hover-social-media');
		$("img", this).replaceWith('<img src="img/mainPage/linkedIn-icon.png">');
	});

	$(".social-icon-facebook").hover(function() {
		$(this).addClass('hover-social-media');
		$("img", this).replaceWith('<img src="img/mainPage/facebook-icon-hover.png">');
	}, function() {
		$(this).removeClass('hover-social-media');
		$("img", this).replaceWith('<img src="img/mainPage/facebook-icon.png">');
	});
});