jQuery(document).ready(function(){

	var outstaffingActive = true;
	var outsourcingActive = false;

	checkOutsourceActive();
	checkOutstaffActive();

	function checkOutstaffActive() {
		if (!outstaffingActive) {
			jQuery(".choose-btn1").css("background-color", "rgba(50, 50, 50, 0.5)");
			jQuery(".outstaff").css({"background-color": "transparent", "border": "1px solid white"});
			jQuery(".article-1").hide();
		}

		else if (outstaffingActive) {
			jQuery(".choose-btn1").css("background-color", "transparent");
			jQuery(".outstaff").css({"background-color": "#ff4d55", "border": "none"});
			jQuery(".article-1").show();
		}
	}

	function checkOutsourceActive() {
		if (!outsourcingActive) {
			jQuery(".choose-btn2").css("background-color", "rgba(50, 50, 50, 0.5)");
			jQuery(".outsource").css({"background-color": "transparent", "border": "1px solid white"});
			jQuery(".article-2").hide();
		}

		else if (outsourcingActive) {
			jQuery(".choose-btn2").css("background-color", "transparent");
			jQuery(".outsource").css({"background-color": "#ff4d55", "border": "none"});
			jQuery(".article-2").show();
		}
	}

	jQuery(".outstaff").click(function() {
		if (!outstaffingActive) {
			outstaffingActive = true;
			outsourcingActive = false;
			checkOutstaffActive();
			checkOutsourceActive();
		}
		else if (outstaffingActive) return
	});

	jQuery(".outsource").click(function() {
		if (!outsourcingActive) {
			outsourcingActive = true;
			outstaffingActive = false;
			checkOutsourceActive();
			checkOutstaffActive();
		}
		else if (outsourcingActive) return
	});

	function htmSlider() {

	  var slideWrap = jQuery('.portfolio-slide-wrap');
	  var nextLink = jQuery('.portfolio-next-slide');
	  var prevLink = jQuery('.portfolio-prev-slide');
	  var is_animate = false;
	  var slideWidth = jQuery('.portfolio-slide-item').outerWidth();
	  var scrollSlider = slideWrap.position().left - slideWidth;
			

	  nextLink.click(function(){
	   if(!slideWrap.is(':animated')) {
	    slideWrap.animate({left: scrollSlider}, 400, function(){
	     slideWrap
	      .find('.portfolio-slide-item:first')
	      .appendTo(slideWrap)
	      .parent()
	      .css({'left': 0});
	    });
	   }
	  });


	  prevLink.click(function(){
	    if(!slideWrap.is(':animated')) {
	      slideWrap
	        .css({'left': scrollSlider})
	        .find('.portfolio-slide-item:last')
	        .prependTo(slideWrap)
	        .parent()
	        .animate({left: 0}, 500);
	    }
	  });

	 }
	 
	 htmSlider();

});

jQuery(document).ready(function() {
 jQuery(".review-slider").each(function () { // обрабатываем каждый слайдер
  var obj = jQuery(".review-slider li");
  jQuery(".review-navigation").append("<div class='review-nav'></div>");
  jQuery(obj).each(function () {
   jQuery(".review-navigation .review-nav").append("<span rel='"+jQuery(this).index()+"'></span>"); // добавляем блок навигации
   jQuery(this).addClass("slider"+jQuery(this).index());
  });
  jQuery(".review-nav").find("span").first().addClass("on"); // делаем активным первый элемент меню
 });
});
function sliderJS (obj, sl) { // slider function
 var ul = jQuery(sl).find("ul"); // находим блок
 var bl = jQuery(sl).find("li.slider"+obj); // находим любой из элементов блока
 var step = jQuery(bl).width(); // ширина объекта
 jQuery(ul).animate({marginLeft: "-"+step*obj}, 500); // 500 это скорость перемотки
}
jQuery(document).on("click", ".review-navigation .review-nav span", function() { // slider click navigate
 var sl = jQuery(this).closest(".reviews"); // находим, в каком блоке был клик
 jQuery(sl).find("span").removeClass("on"); // убираем активный элемент
 jQuery(this).addClass("on"); // делаем активным текущий
 var obj = jQuery(this).attr("rel"); // узнаем его номер
 sliderJS(obj, sl); // слайдим
 return false;
});