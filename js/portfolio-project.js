
jQuery(document).ready(function(){
 function htmSlider() {

  var slideWrap = jQuery('.slide-wrap');
  var nextLink = jQuery('.next-slide');
  var prevLink = jQuery('.prev-slide');
  var is_animate = false;
  var slideWidth = jQuery('.slide-item').outerWidth();
  var scrollSlider = slideWrap.position().left - slideWidth;
		

  nextLink.click(function(){
   if(!slideWrap.is(':animated')) {
    slideWrap.animate({left: scrollSlider}, 400, function(){
     slideWrap
      .find('.slide-item:first')
      .appendTo(slideWrap)
      .parent()
      .css({'left': 0});
    });
   }
  });


  prevLink.click(function(){
    if(!slideWrap.is(':animated')) {
      slideWrap
        .css({'left': scrollSlider})
        .find('.slide-item:last')
        .prependTo(slideWrap)
        .parent()
        .animate({left: 0}, 500);
    }
  });

 }

  var elems = [];

  $(".slide-item").each( function(index, element) {
    elems.push($(element).attr("id"));
  });

  $(".slide-item").hover(function() {
    for (var i = 1; i <= elems.length; i++) {
      if ($(this).attr("id") == "index-"+i) {
        var hover = "#hover-"+i;
        $(hover).addClass('opacity-1');
      } else continue
    }
  }, function() {
    $(".hover-item").removeClass('opacity-1');
  });
 
 htmSlider();
});
