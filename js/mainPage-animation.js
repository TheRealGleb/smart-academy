jQuery(document).ready(function() {

	$(".portfolio").hover(function() {
		$(".portfolio-text", this).addClass('hover-transition');
		$("span", this).addClass('hover-transition-span');
	}, function() {
		$(".portfolio-text", this).removeClass('hover-transition');
		$("span", this).removeClass('hover-transition-span');
	});

	$(".services").hover(function() {
		$(".portfolio-text", this).addClass('hover-transition');
		$("span", this).addClass('hover-transition-span');
	}, function() {
		$(".portfolio-text", this).removeClass('hover-transition');
		$("span", this).removeClass('hover-transition-span');
	});

	$(".career").hover(function() {
		$(".career-text", this).addClass('hover-transition');
		$("span", this).addClass('hover-transition-span2');
	}, function() {
		$(".career-text", this).removeClass('hover-transition');
		$("span", this).removeClass('hover-transition-span2');
	});

	$(".our-team").hover(function() {
		$(".career-text", this).addClass('hover-transition');
		$("span", this).addClass('hover-transition-span2');
	}, function() {
		$(".career-text", this).removeClass('hover-transition');
		$("span", this).removeClass('hover-transition-span2');
	});

	$(".blog").hover(function() {
		$(".career-text", this).addClass('hover-transition');
		$("span", this).addClass('hover-transition-span2');
	}, function() {
		$(".career-text", this).removeClass('hover-transition');
		$("span", this).removeClass('hover-transition-span2');
	});

	$(".contacts").hover(function() {
		$(".career-text", this).addClass('hover-transition');
		$("span", this).addClass('hover-transition-span2');
	}, function() {
		$(".career-text", this).removeClass('hover-transition');
		$("span", this).removeClass('hover-transition-span2');
	});

});


